FROM alpine:3.13.5

MAINTAINER Docker.Subgroup <mail@hotmail.com>

RUN apk update && apk upgrade 

RUN apk add --no-cache bash

RUN apk --no-cache add ruby ruby-dev ruby-bundler ruby-json ruby-irb ruby-rake ruby-bigdecimal

RUN rm -rf /var/cache/apk/*

RUN mkdir develop

VOLUME /develop

COPY ./ /develop/

CMD ["sh", "/develop/compile.sh"]

