# Repository Best Practices

### Strating the repository
1 Know the files that are about to be uploaded. Select the code or files that aren't necessary to be uplodaded, not even ignored.

2 Initialize Git repository with ``Git init``.

3 Create a .gitignore file to ignore the dependencies or binary files to be uploaded.

4 Create a README.md file to share the documentation of the project. It can be blank or just with a title in case of a new project.

5 Add remote to the git project.

6 Commit the initial project repository.
- start the commit message like _"Initial Commit - State of the Project - Features Performed"_

7 Upload with git push.

8 Start a Workflow
    
## GitFlow

GitFlow helps to order the code in branches to have a flow of work parsed in commits.

| Branch | Purpose |
| ------ | ------ |
| Master | History of the oficial Release |
| HotFix | Maintenance branch created to make last minute modifications |
| Release | Publishing Branch dedicated branch to prepare releases |
| Develop | Record of Features integrations |
| INI/Feature | Purpose of integrating the functionalities to the develop |
| DEV/Feature | These branches have the features for a release |

![WorkFlow](https://i.pinimg.com/originals/2a/5d/5c/2a5d5c1d5f42a3d0fd712ae7e4b23824.png)

## Resist a blind git add
If you do use "git add ." , review what’s in staging before you push. If you see an unfamiliar object in your project folder when you do a git status, find out where it came from and why it’s still in your project directory after you’ve run a make clean or equivalent command. It’s a rare build artifact that won’t regenerate during compilation, so think twice before committing it.

## Document
Human Memory is fragile, document how to install, how it's implemented, how to run, stop, debug, etc. This will help your future self and future developers that might work with the repository.
> Successful documentation will make information easily accessible, provide a limited number of user entry points, help new users learn quickly, simplify the product and help cut support costs.

## Large File Storage

It does not keep all versions of a file on your machine. Instead, it only provides the files you actually need in your checked out revision. If you switch branches, it will automatically check if you need a specific version of such a big file and get it for you - on demand. It uses Pointers Instead of Real Data
![LFS](https://www.git-tower.com/learn/git/ebook/en/command-line/advanced-topics/git-lfs/03-setup-git-lfs.png)
